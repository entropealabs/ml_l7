defmodule MlL7 do
  @moduledoc """
  Documentation for `MlL7`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> MlL7.hello()
      :world

  """
  def hello do
    :world
  end
end
