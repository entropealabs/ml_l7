# ML-L7

A simple case for the Nikon ML-L7 to mount to a tripod handle.


![OpenScad Render](./images/open_scad.png)
![Wireframe](./images/wireframe.png)
![Cura Setup](./images/cura.png)
![Installed](./images/installed.jpg)
