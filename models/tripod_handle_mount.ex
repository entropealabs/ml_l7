defmodule TripodHandleMount do
  use OpenSCAD

  @remote_width 35
  @remote_length 80
  @remote_height 7.35

  @handle_diameter 15

  @wall_thickness 2

  def main() do
    outer_case =
      cube(
        size: [
          @remote_length + @wall_thickness * 2,
          @remote_height + @wall_thickness * 2,
          @remote_width + @wall_thickness * 2
        ],
        center: true
      )

    inner1 = cube(size: [@remote_length, @remote_height, @remote_width], center: true)

    inner2 =
      cube(
        size: [
          @remote_length - @wall_thickness * 2,
          @wall_thickness + 5,
          @remote_width - @wall_thickness * 2
        ],
        center: true
      )
      |> translate(v: [0, @remote_height / 2 + @wall_thickness, 0])

    inner_case = union([inner1, inner2])

    mount_block =
      cube(
        size: [@remote_length + @wall_thickness * 2, @handle_diameter - 10, @handle_diameter - 2],
        center: true
      )
      |> translate(v: [0, -(@handle_diameter / 2), 0])

    strap1 =
      cube(size: [6, 2, 20], center: true)
      |> translate(v: [(@remote_length - 5) / 2 * -1, -(@wall_thickness * 2 + 2), 0])

    strap2 =
      cube(size: [6, 2, 20], center: true)
      |> translate(v: [(@remote_length - 5) / 2, -(@wall_thickness * 2 + 2), 0])

    mount_groove =
      cylinder(h: @remote_length + @wall_thickness * 2 + 2, d: @handle_diameter)
      |> rotate(a: 90, v: [0, 1, 0])
      |> translate(
        v: [
          (@remote_length + @wall_thickness * 2 + 2) / 2 * -1,
          -(@wall_thickness + @handle_diameter - 3),
          0
        ]
      )

    mount = mount_block |> difference(mount_groove) |> difference(strap1) |> difference(strap2)

    outer_case = union([outer_case, mount])

    # union([outer_case, inner_case])
    outer_case
    |> difference(inner_case)
    |> write("tripod_handle_mount.scad")
  end
end
