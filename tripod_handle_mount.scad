difference(){
  union(){
    cube(center = true, size = [84, 11.35, 39]);
    difference(){
      difference(){
        difference(){
          translate(v = [0, -7.5, 0]){
            cube(center = true, size = [84, 5, 13]);
          }
          translate(v = [-43.0, -14, 0]){
            rotate(a = 90, v = [0, 1, 0]){
              cylinder(d = 15, h = 86);
            }
          }
        }
        translate(v = [-37.5, -6, 0]){
          cube(center = true, size = [6, 2, 20]);
        }
      }
      translate(v = [37.5, -6, 0]){
        cube(center = true, size = [6, 2, 20]);
      }
    }
  }
  union(){
    cube(center = true, size = [80, 7.35, 35]);
    translate(v = [0, 5.675, 0]){
      cube(center = true, size = [76, 7, 31]);
    }
  }
}
